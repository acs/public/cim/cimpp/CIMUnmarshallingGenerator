#include "myastvisitor.h"
#include <iostream>
#include <fstream>
#include <regex>
#include <clang/Tooling/Refactoring.h> // clang::tooling::Replacements

using namespace clang;
using namespace clang::tooling;

unsigned int MyASTVisitor::field_counter;
unsigned int MyASTVisitor::class_counter;
unsigned int MyASTVisitor::enum_counter;
unsigned int MyASTVisitor::association_counter;
ctemplate::TemplateDictionary MyASTVisitor::assignments_dict("assignments_dict");
ctemplate::TemplateDictionary MyASTVisitor::factory_dict("factory_dict");
ctemplate::TemplateDictionary MyASTVisitor::task_cpp_dict("task_cpp_dict");


MyASTVisitor::MyASTVisitor(const ASTContext& Context, const SourceManager& SrcManager)
	:	Context(Context),
		_SourceManager(SrcManager)
{
}

bool MyASTVisitor::VisitDecl(Decl *D)
{
	// Für das Verständnis dieser Funktion bitte das Flowchart beachten!!!
	// Betrachte alle Feld Deklarationen
	// Felder sind z.B. Attribute in Klassen
	if(FieldDecl* fieldDecl = dyn_cast<FieldDecl>(D))
	{
		// Betrachte die Enum felder
		if(fieldDecl->getType()->isEnumeralType())
		{
			// Generiere einen Template Block
			// Wir brauchen ein Wörterbuch für die Template Engine
			ctemplate::TemplateDictionary* new_assignment = assignments_dict.AddSectionDictionary("ASSIGNMENT");
			// Schreibe die werte in das dictionary für eine neue Zuweisung
			new_assignment->SetValue("CLASS_NAME", fieldDecl->getParent()->getNameAsString());
			new_assignment->SetValue("QUAL_CLASS_NAME", dyn_cast<CXXRecordDecl>(fieldDecl->getParent())->getQualifiedNameAsString());
			new_assignment->SetValue("FIELD_NAME", fieldDecl->getNameAsString());
			new_assignment->ShowSection("IS_NOT_STRING");
			field_counter++;
		}


		// Betrachte die Pointer (Composition and Aggregation)
		if(fieldDecl->getType().getTypePtr()->isAnyPointerType())
		{
			// Fülle Wörterbuch für das Template
			ctemplate::TemplateDictionary* new_assign_ptr_func = task_cpp_dict.AddSectionDictionary("ASSIGN_PTR");
			new_assign_ptr_func->SetValue("CLASS_NAME", dyn_cast<CXXRecordDecl>(fieldDecl->getParent())->getNameAsString());
			new_assign_ptr_func->SetValue("FIELD_NAME", fieldDecl->getNameAsString());
			new_assign_ptr_func->SetValue("QUAL_CLASS_NAME", dyn_cast<CXXRecordDecl>(fieldDecl->getParent())->getQualifiedNameAsString());

			std::string field_qual_type(FullSourceLoc(fieldDecl->getLocStart(), _SourceManager).getCharacterData());
			field_qual_type = field_qual_type.substr(0, field_qual_type.find_first_of(" *"));
			new_assign_ptr_func->SetValue("FIELD_QUAL_TYPE", field_qual_type);
			association_counter++;
		}


		// Betrachte die Listen (Composition and Aggregation)
		std::string str = fieldDecl->getType().getAsString();
		std::regex expr("std::list<([a-zA-Z0-9:]*)[ *]*>");
		std::smatch m;
		if(std::regex_match(str, m, expr))
		{
			ctemplate::TemplateDictionary* new_assign_list_func = task_cpp_dict.AddSectionDictionary("ASSIGN_LIST");
			new_assign_list_func->SetValue("CLASS_NAME", dyn_cast<CXXRecordDecl>(fieldDecl->getParent())->getNameAsString());
			new_assign_list_func->SetValue("FIELD_NAME", fieldDecl->getNameAsString());
			new_assign_list_func->SetValue("QUAL_CLASS_NAME", dyn_cast<CXXRecordDecl>(fieldDecl->getParent())->getQualifiedNameAsString());
			new_assign_list_func->SetValue("FIELD_QUAL_TYPE", std::string(m[1]));
			association_counter++;
		}


		// Betrachte alle anderen Felder
		else
		{
			// Bahandle alle Felder die primitive Datentypen haben
			if(fieldDecl->getType().getAsString() == "IEC61970::Base::Domain::Boolean" ||
				fieldDecl->getType().getAsString() == "IEC61970::Base::Domain::Decimal" || // NOTE: Decimal ist offiziell kein Primitiver Datentyp, wird aber in der regel als solcher behandelt!
				fieldDecl->getType().getAsString() == "IEC61970::Base::Domain::Float" ||
				fieldDecl->getType().getAsString() == "IEC61970::Base::Domain::Integer" ||
				fieldDecl->getType().getAsString() == "IEC61970::Base::Domain::String")
			{
				// Ist der Parent, d.h. die das Feld enthaltende Klasse, nicht auf der Whitelist
				if(whitelist.find(dyn_cast<CXXRecordDecl>(fieldDecl->getParent())->getQualifiedNameAsString()) == whitelist.end())
				{
					// Generiere einen Template Block
					// Wir brauchen ein Wörterbuch für die Template Engine
					ctemplate::TemplateDictionary* new_assignment = assignments_dict.AddSectionDictionary("ASSIGNMENT");
					// Schreibe die werte in das dictionary für eine neue Zuweisung
					new_assignment->SetValue("CLASS_NAME", fieldDecl->getParent()->getNameAsString());
					new_assignment->SetValue("QUAL_CLASS_NAME", dyn_cast<CXXRecordDecl>(fieldDecl->getParent())->getQualifiedNameAsString());
					new_assignment->SetValue("FIELD_NAME", fieldDecl->getNameAsString());

					// if datatype is Domain::String show correct section
					if(fieldDecl->getType().getAsString().find("Domain::String") != std::string::npos)
					{
						new_assignment->ShowSection("IS_STRING");
					}
					else
					{
						new_assignment->ShowSection("IS_NOT_STRING");
					}

					field_counter++;
				}
			}
			else
			{
				// Ist der Typ des Felds selbst auf der Whitelist?
				// --> Dann generiere Code
				if(whitelist.find(fieldDecl->getType().getAsString()) != whitelist.end())
				{
					// Generiere einen Template Block
					// Wir brauchen ein Wörterbuch für die Template Engine
					ctemplate::TemplateDictionary* new_assignment = assignments_dict.AddSectionDictionary("ASSIGNMENT");
					// Schreibe die werte in das dictionary für eine neue Zuweisung
					new_assignment->SetValue("CLASS_NAME", fieldDecl->getParent()->getNameAsString());
					new_assignment->SetValue("QUAL_CLASS_NAME", dyn_cast<CXXRecordDecl>(fieldDecl->getParent())->getQualifiedNameAsString());
					new_assignment->SetValue("FIELD_NAME", fieldDecl->getNameAsString());
					new_assignment->SetValue("VALUE", ".value");

					// Find datatype of value field
					FieldDecl* ValueField;
					for(FieldDecl* field : fieldDecl->getType()->getAsCXXRecordDecl()->fields())
					{
						if(field->getNameAsString() == "value")
						{
							ValueField = field;
						}
					}
					// if datatype is Domain::String show correct section
					if(ValueField->getType().getAsString().find("Domain::String") != std::string::npos)
					{
						new_assignment->ShowSection("IS_STRING");
					}
					else
					{
						new_assignment->ShowSection("IS_NOT_STRING");
					}

					field_counter++;
				}
			}
		}
	}


	// Finde alle Klassendefinitionen
	if(CXXRecordDecl* RecDecl = dyn_cast<CXXRecordDecl>(D))
	{
		// An dieser Stelle werden nur die Definitionen betrachtet. Nicht die Deklarationen!
		if(RecDecl->hasDefinition())
		{
			// Zähle die Klassen, die nicht auf der Whitelist sind
			if(whitelist.find(RecDecl->getQualifiedNameAsString()) == whitelist.end())
			{
				// Lege eine neue Section in CIMFactory an
				ctemplate::TemplateDictionary* section_dict = factory_dict.AddSectionDictionary("FACTORY");
				section_dict->SetValue("CLASS_NAME", RecDecl->getNameAsString());
				section_dict->SetValue("QUAL_CLASS_NAME", RecDecl->getQualifiedNameAsString());
				class_counter++;
			}
		}
	}


	// Finde alle Enum Class Definitionen
	if(EnumDecl* enumDecl = dyn_cast<EnumDecl>(D))
	{
		enum_counter++;
		// Lege eine neue Section für assignments an
		// Section ENUM_STRINGSTREAM
		ctemplate::TemplateDictionary* new_enum_getter = assignments_dict.AddSectionDictionary("ENUM_STRINGSTREAM");
		new_enum_getter->SetValue("QUAL_ENUM_CLASS_TYPE", enumDecl->getQualifiedNameAsString());
		new_enum_getter->SetValue("ENUM_CLASS_TYPE", enumDecl->getNameAsString());

		// Lege für jedes Symbol einen neuen if block im Template an
		for(EnumConstantDecl* enumSymbol : enumDecl->enumerators())
		{
			// Section IF_SYMBOL
			ctemplate::TemplateDictionary* new_symbol = new_enum_getter->AddSectionDictionary("IF_SYMBOL");
			new_symbol->SetValue("SYMBOL", enumSymbol->getNameAsString());
		}
	}
	return true;
}


// Diese Funktion bleibt ungenutzt, muss aber trotzdem definiert sein!
bool MyASTVisitor::VisitStmt(Stmt *stmt)
{
	return true;
}


// Load whitelist
std::unordered_set<std::string> MyASTVisitor::whitelist;

void MyASTVisitor::load_whitelist(const std::string file_name)
{
	std::ifstream file(file_name);
	std::string line;
	while (std::getline(file, line))
	{
		whitelist.insert(line);
	}
	return;
}

