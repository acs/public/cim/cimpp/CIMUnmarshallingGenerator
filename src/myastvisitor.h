#ifndef MYASTVISITOR_H
#define MYASTVISITOR_H

#include <clang/AST/RecursiveASTVisitor.h>
#include <ctemplate/template.h>
#include <unordered_set>

// The visitor approach
class MyASTVisitor : public clang::RecursiveASTVisitor<MyASTVisitor>
{
public:
	const clang::ASTContext& Context;
	const clang::SourceManager& _SourceManager;

	explicit MyASTVisitor(const clang::ASTContext& Context, const clang::SourceManager& SrcManager);
	bool VisitDecl(clang::Decl *D);
	bool VisitStmt(clang::Stmt *stmt);

	static void load_whitelist(const std::string file_name);

	static std::unordered_set<std::string> whitelist;
	static unsigned int field_counter;
	static unsigned int class_counter;
	static unsigned int enum_counter;
	static unsigned int association_counter;
	static ctemplate::TemplateDictionary assignments_dict;
	static ctemplate::TemplateDictionary factory_dict;
	static ctemplate::TemplateDictionary task_cpp_dict;
};

#endif // MYASTVISITOR_H
