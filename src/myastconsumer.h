#ifndef MYASTCONSUMER_H
#define MYASTCONSUMER_H

#include <clang/AST/ASTConsumer.h>
#include "myastvisitor.h"

#include <unordered_set>

class MyASTConsumer : public clang::ASTConsumer
{
public:
	// Methods
	explicit MyASTConsumer(const clang::ASTContext& Context, const clang::SourceManager& SrcManager);

	virtual bool HandleTopLevelDecl(clang::DeclGroupRef DR) override;

	// Attributes
	const clang::SourceManager& _srcManager;
	static std::unordered_set<std::string> locations;
	MyASTVisitor Visitor;
};

#endif // MYASTCONSUMER_H
