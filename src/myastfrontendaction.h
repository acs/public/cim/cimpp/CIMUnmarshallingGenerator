#ifndef MYASTFRONTENDACTION_H
#define MYASTFRONTENDACTION_H

#include <clang/Frontend/FrontendAction.h>
#include <clang/Frontend/CompilerInstance.h>

class MyASTFrontendAction : public clang::ASTFrontendAction
{
public:
	MyASTFrontendAction();

	virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(clang::CompilerInstance &CI, llvm::StringRef file) override;
};

#endif // MYASTFRONTENDACTION_H
