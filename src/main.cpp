#include "clang/Tooling/Tooling.h"
#include "boost/filesystem.hpp"
#include "ctemplate/template.h"

#include "myastvisitor.h"
#include "myastconsumer.h"
#include "myastfrontendaction.h"

#include <iostream>
#include <fstream>
#include <chrono>

// Globals
std::string search_path;
std::unordered_set<std::string> files_set;
std::string binary_path;

int main(int argc, const char **argv)
{
	// Set current path
	binary_path = argv[0];
	binary_path = binary_path.substr(0, binary_path.find_last_of("/\\") + 1);
	binary_path = boost::filesystem::absolute(binary_path).string();

	// Check for arguments
	if(argc <= 1)
	{
		std::cerr << "Too few arguments:\nUsage: CIMParserGenerator <EACode_path>" << std::endl;
		exit(1);
	}
	// Find all relevant files
	std::vector<std::string> files;
	search_path = argv[1];
	// Find all files in search_path
	boost::filesystem::path p(search_path);
	try
	{
		p = boost::filesystem::canonical(p);
		for(boost::filesystem::recursive_directory_iterator it(p); it != boost::filesystem::recursive_directory_iterator(); it++)
		{
			if(boost::filesystem::is_regular_file(*it) && (it->path().extension() == ".h"))
			{
				files.push_back(it->path().string());
				files_set.insert(it->path().string());
			}
		}
	}
	catch(const boost::filesystem::filesystem_error& ex)
	{
		std::cout << ex.what() << std::endl;
	}

	// Prepare compilation database
	std::vector<std::string> args;
	args.push_back(std::string("-I").append(p.string()));
	args.push_back("-I/Users/daniel/Bachelorarbeit/Git/CIMUnmarshallingGenerator/cmake_build/resource/DummySTL");
	args.push_back(CLANG_INC);
	args.push_back("-xc++");
	args.push_back("-std=c++11");

	clang::tooling::FixedCompilationDatabase db(argv[1], args);
	clang::tooling::ClangTool Tool(db, files);

	// Load whitelist for MyASTVisitor
	MyASTVisitor::load_whitelist(binary_path + "resource/whitelist.txt");

	// Timer start
	std::chrono::time_point<std::chrono::high_resolution_clock> start, stop;
	start = std::chrono::high_resolution_clock::now();

	Tool.run(clang::tooling::newFrontendActionFactory<MyASTFrontendAction>().get());

	// Timer stop
	stop = std::chrono::high_resolution_clock::now();
	auto dur = stop - start;
	std::cout << std::chrono::duration_cast<std::chrono::seconds>(dur).count() << "s" << std::endl;

	std::cout << "field_counter = " << MyASTVisitor::field_counter << std::endl;
	std::cout << "class_counter = " << MyASTVisitor::class_counter << std::endl;
	std::cout << "enum_counter = " << MyASTVisitor::enum_counter << std::endl;
	std::cout << "association_counter = " << MyASTVisitor::association_counter << std::endl;

	// Write to file
	std::string output;
	ctemplate::ExpandTemplate(binary_path + "resource/assignments.cpp.tpl", ctemplate::DO_NOT_STRIP, &MyASTVisitor::assignments_dict, &output);
	std::ofstream file;
	file.open("assignments.cpp", std::fstream::trunc);
	if(file.good())
		file << output;
	file.close();

	output.clear();
	ctemplate::ExpandTemplate(binary_path + "resource/Task.cpp.tpl", ctemplate::DO_NOT_STRIP, &MyASTVisitor::task_cpp_dict, &output);
	file.open("Task.cpp", std::fstream::trunc);
	if(file.good())
		file << output;
	file.close();

	output.clear();
	ctemplate::ExpandTemplate(binary_path + "resource/CIMFactory.cpp.tpl", ctemplate::DO_NOT_STRIP, &MyASTVisitor::factory_dict, &output);
	file.open("CIMFactory.cpp", std::fstream::trunc);
	if(file.good())
		file << output;
	file.close();

	boost::filesystem::copy_file(binary_path + "resource/assignments.hpp.tpl", "assignments.hpp", boost::filesystem::copy_option::overwrite_if_exists);
	boost::filesystem::copy_file(binary_path + "resource/CIMFactory.hpp.tpl", "CIMFactory.hpp", boost::filesystem::copy_option::overwrite_if_exists);
	boost::filesystem::copy_file(binary_path + "resource/Task.hpp.tpl", "Task.hpp", boost::filesystem::copy_option::overwrite_if_exists);
	
	return 0;
}
