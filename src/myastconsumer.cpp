#include "myastconsumer.h"

#include <iostream>
#include <regex>

using namespace clang;

extern std::string search_path;
extern std::unordered_set<std::string> files_set;

std::unordered_set<std::string> MyASTConsumer::locations = std::unordered_set<std::string>();

MyASTConsumer::MyASTConsumer(const ASTContext& Context, const SourceManager& SrcManager)
	:  _srcManager(SrcManager),
		Visitor(Context,SrcManager)
{
}

bool MyASTConsumer::HandleTopLevelDecl(DeclGroupRef DR)
{
	for (DeclGroupRef::iterator b = DR.begin(), e = DR.end(); b != e; ++b)
	{
		// Is the file one of the input files?
		std::string location = (*b)->getLocation().printToString(_srcManager);
		size_t pos = location.find(':');
		std::string FileName = (*b)->getLocation().printToString(_srcManager).substr(0, pos);

		std::unordered_set<std::string>::const_iterator got = files_set.find(FileName);
		if(got != files_set.end())
		{
			// Did we already visit this location? If not visit now and never again.
			got = locations.find(location);
			if(got == locations.end())
			{
				Visitor.TraverseDecl(*b);
				//(*b)->dumpColor();
				locations.insert(location);
			}
		}
	}
	return true;
}
