# CIMUnmarshallingGenerator

This tool is used to generate most of the CIM parser source files.

##### Dependencies:
* Boost >= 1.60.0
* llvm/clang >= 3.6.2
* ctemplate >= 2.3

Other versions of the libraries might work, but are currently untested.

#### Build instructions:
1. Manually configure Makefile using a text editor

  Therefore you will need to find the C++ standard library header on your
  system. On linux machines they are usually placed in
  `/usr/include/c++/<version>/` when using gcc.

2. Build with `make`

For information about how to build clang from source please refer to the
[getting started guide](http://clang.llvm.org/get_started.html) of clang and the
[quick start guide](http://llvm.org/docs/CMake.html#quick-start) of llvm.


#### Usage:
`./CIMUnmarshallingGenerator <GenratedCodeDir>`

The tool generates the following files: `assignments.h/.cpp`, `task.h/.cpp`,
`CIMFactory.h/.cpp`.
