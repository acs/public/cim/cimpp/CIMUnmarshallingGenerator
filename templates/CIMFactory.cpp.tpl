#include "CIMFactory.hpp"
#include "Folders.hpp"

static std::unordered_map<std::string, BaseClass* (*)()> initialize();
std::unordered_map<std::string, BaseClass* (*)()> CIMFactory::factory_map = initialize();

BaseClass* CIMFactory::CreateNew(const std::string& name)
{
	std::unordered_map<std::string, BaseClass* (*)()>::iterator it = factory_map.find(name);
	if(it != factory_map.end())
		return (*it->second)();
	else
		return nullptr;
}

bool CIMFactory::IsCIMClass(const std::string& name)
{
	std::unordered_map<std::string, BaseClass* (*)()>::iterator it = factory_map.find(name);
	if(it == factory_map.end())
		return false;
	else
		return true;
}

{{#FACTORY}}
// cim:{{CLASS_NAME}}
BaseClass* {{CLASS_NAME}}_factory()
{
	return new {{QUAL_CLASS_NAME}};
}
{{/FACTORY}}
static std::unordered_map<std::string, BaseClass* (*)()> initialize()
{
	std::unordered_map<std::string, BaseClass* (*)()> map;

	{{#FACTORY}}map.insert(std::make_pair("cim:{{CLASS_NAME}}", &{{CLASS_NAME}}_factory));
	{{/FACTORY}}
	return map;
}
