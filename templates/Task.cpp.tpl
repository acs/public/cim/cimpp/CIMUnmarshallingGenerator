#include <iostream>

#include "Folders.hpp"
#include "Aliases.hpp"
#include "Task.hpp"

typedef bool (*task_function)(BaseClass*, BaseClass*);
static std::unordered_map<std::string, bool (*)(BaseClass*, BaseClass*)> initialize();
std::unordered_map<std::string, bool (*)(BaseClass*, BaseClass*)> Task::dynamic_switch = initialize();

Task::Task()
{
}

Task::Task(BaseClass* CIMObj, const std::string CIMAttrName, const std::string Value)
	: _CIMObj(CIMObj), _CIMAttrName(CIMAttrName), _Value(Value)
{
}

Task::~Task()
{
}

void Task::print()
{
	if(IEC61970::Base::Core::IdentifiedObject* IdObj = dynamic_cast<IEC61970::Base::Core::IdentifiedObject*>(_CIMObj))
		std::cout << _CIMAttrName << " '" << IdObj->name << "' = '" << _Value << "'" << std::endl;
	else
		std::cout << _CIMAttrName << " = '" << _Value << "'" << std::endl;

}

bool Task::resolve(std::unordered_map<std::string, BaseClass*> *RDFMap)
{
	std::unordered_map<std::string, BaseClass*>::iterator it_id = RDFMap->find(_Value);
	if(it_id == RDFMap->end())
		return false;

	std::unordered_map<std::string, bool (*)(BaseClass*, BaseClass*)>::iterator it_func = dynamic_switch.find(_CIMAttrName);
	if(it_func == dynamic_switch.end())
		return false;

	if((*it_func->second)(_CIMObj, it_id->second))
		return true;
	else
		return (*it_func->second)(it_id->second, _CIMObj);
}

{{#ASSIGN_PTR}}
bool assign_{{CLASS_NAME}}_{{FIELD_NAME}}(BaseClass* BaseClass_ptr1, BaseClass* BaseClass_ptr2)
{
	if({{QUAL_CLASS_NAME}}* element = dynamic_cast<{{QUAL_CLASS_NAME}}*>(BaseClass_ptr1))
	{
		element->{{FIELD_NAME}} = dynamic_cast<{{FIELD_QUAL_TYPE}}*>(BaseClass_ptr2);
		if(element->{{FIELD_NAME}} != nullptr)
			return true;
	}
	return false;
}
{{/ASSIGN_PTR}}

{{#ASSIGN_LIST}}
bool assign_{{CLASS_NAME}}_{{FIELD_NAME}}(BaseClass* BaseClass_ptr1, BaseClass* BaseClass_ptr2)
{
	if({{QUAL_CLASS_NAME}}* element = dynamic_cast<{{QUAL_CLASS_NAME}}*>(BaseClass_ptr1))
	{
		if(dynamic_cast<{{FIELD_QUAL_TYPE}}*>(BaseClass_ptr2) != nullptr)
		{
			element->{{FIELD_NAME}}.push_back(dynamic_cast<{{FIELD_QUAL_TYPE}}*>(BaseClass_ptr2));
			return true;
		}
	}
	return false;
}
{{/ASSIGN_LIST}}

static std::unordered_map<std::string, bool (*)(BaseClass*, BaseClass*)> initialize()
{
	std::unordered_map<std::string, bool (*)(BaseClass*, BaseClass*)> map;

	{{#ASSIGN_PTR}}map.insert(std::make_pair("cim:{{CLASS_NAME}}.{{FIELD_NAME}}", &assign_{{CLASS_NAME}}_{{FIELD_NAME}}));
	map.insert(std::make_pair("cim:{{FIELD_NAME}}.{{CLASS_NAME}}", &assign_{{CLASS_NAME}}_{{FIELD_NAME}}));
	{{/ASSIGN_PTR}}
	{{#ASSIGN_LIST}}map.insert(std::make_pair("cim:{{CLASS_NAME}}.{{FIELD_NAME}}", &assign_{{CLASS_NAME}}_{{FIELD_NAME}}));
	map.insert(std::make_pair("cim:{{FIELD_NAME}}.{{CLASS_NAME}}", &assign_{{CLASS_NAME}}_{{FIELD_NAME}}));
	{{/ASSIGN_LIST}}

#include "AliasesTask.hpp"

	load_aliases<task_function>(map, "task_alias.csv");

	return map;
}
