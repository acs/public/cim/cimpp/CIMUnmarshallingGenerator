#include <sstream>
#include <utility>
#include <unordered_map>

#include "Folders.hpp"
#include "Aliases.hpp"
#include "assignments.hpp"

typedef bool (*assign_function)(std::stringstream&, BaseClass*);
static std::unordered_map<std::string, assign_function> dynamic_switch_factory();
static std::unordered_map<std::string, assign_function> dynamic_switch = dynamic_switch_factory();

bool assign(BaseClass* CIMObj, const std::string& CIMAttrName, const std::string& Value)
{
	std::unordered_map<std::string, assign_function>::iterator it = dynamic_switch.find(CIMAttrName);
	if(it == dynamic_switch.end())
		return false;
	std::stringstream str;
	str << Value;
	return (*it->second)(str, CIMObj);
}

{{#ENUM_STRINGSTREAM}}
std::istream& operator>>(std::istream& lop, {{QUAL_ENUM_CLASS_TYPE}}& rop)
{
	std::string EnumSymbol;
	lop >> EnumSymbol;
	size_t pos = EnumSymbol.find_first_of('.');
	if(EnumSymbol.substr(0, pos) != "{{ENUM_CLASS_TYPE}}")
	{
		lop.setstate(std::ios::failbit);
		return lop;
	}

	EnumSymbol = EnumSymbol.substr(pos + 1);
	{{#IF_SYMBOL}}
	if(EnumSymbol == "{{SYMBOL}}")
	{
		rop = {{QUAL_ENUM_CLASS_TYPE}}::{{SYMBOL}};
		return lop;
	}
	{{/IF_SYMBOL}}
	lop.setstate(std::ios::failbit);
	return lop;
}
{{/ENUM_STRINGSTREAM}}

{{#ASSIGNMENT}}
// cim:{{CLASS_NAME}}.{{FIELD_NAME}}
bool assign_{{CLASS_NAME}}_{{FIELD_NAME}}(std::stringstream& buffer, BaseClass* base_class_ptr)
{
	if({{QUAL_CLASS_NAME}}* element = dynamic_cast<{{QUAL_CLASS_NAME}}*>(base_class_ptr))
	{
		{{#IS_NOT_STRING}}buffer >> element->{{FIELD_NAME}}{{VALUE}};{{/IS_NOT_STRING}}{{#IS_STRING}}element->{{FIELD_NAME}}{{VALUE}} = buffer.str();{{/IS_STRING}}
		if(buffer.fail())
			return false;
		else
			return true;
	}
	else
		return false;
}
{{/ASSIGNMENT}}

// Dynamic Switch Factory
std::unordered_map<std::string, assign_function> dynamic_switch_factory()
{
	std::unordered_map<std::string, assign_function> dynamic_switch;

	{{#ASSIGNMENT}}dynamic_switch.insert(std::make_pair("cim:{{CLASS_NAME}}.{{FIELD_NAME}}", &assign_{{CLASS_NAME}}_{{FIELD_NAME}}));
	{{/ASSIGNMENT}}

#include "AliasesAssignment.hpp"

	load_aliases<assign_function>(dynamic_switch, "assignment_alias.csv");

	return dynamic_switch;
}
